package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public double getBookPrice(String celsius) {
        return Double.parseDouble(celsius)*1.8+32;
    }
}

